angular.module("textAngularDemo", ['textAngular']);

function demoController($scope) {
	// $scope.htmlContent = '<h2>Try me!</h2><p>textAngular is a super cool WYSIWYG Text Editor directive for AngularJS</p><p><b>Features:</b></p><ol><li>Automatic Seamless Two-Way-Binding</li><li style="color: blue;">Super Easy <b>Theming</b> Options</li><li>Simple Editor Instance Creation</li><li>Safely Parses Html for Custom Toolbar Icons</li><li>Doesn&apos;t Use an iFrame</li><li>Works with Firefox, Chrome, and IE8+</li></ol><p><b>Code at GitHub:</b> <a href="https://github.com/fraywing/textAngular">Here</a> </p>';
	$scope.htmlContent = '<h4>Try me!</h4><p>textAngular is a AngularJS</p><p><b>Features:</b></p><ol><li>Automatic Seamless Two-Way-Binding</li><li style="color: blue;">Super Easy <b>Theming</b> Options</li></ol><p><b>Code at GitHub:</b> <a href="https://github.com/fraywing/textAngular">Here</a> </p>';
}

// either:
// * transmit event: insertText and insert it on current cursor pos
// * read current position from event ?!

function getLine(node) {
	// TODO returns the same value for all <ol> ! Either:
	// remove lists feature
	// provide COMPLETE path f.e. [3,1] -> list in line 3, list item 1, till TextNode
	var child;
	while (node.parentNode && !child) {
		var n = node.parentNode;
		var attrs = n.attributes;
		for (var i = 0; attrs && i < attrs.length; i++) {
			if (attrs[i].name === 'contenteditable') {
				child = node;
			}
		}
		node = n;
	}

	// if everything is ok:
	// * node := our contenteditable
	// * child := line containing the original node
	if (child) {
		for (var i = 0; i < node.children.length; i++) {
			if (node.children[i] === child) {
				return i;
			}
		}
	}
	return -1;
}

function keyDownInterceptor(e) {
	// e.preventDefault();
	// console.dir(e)
	// TODO reports before event takes place - oboslete data ! ( move to keyUp)
	// console.log('_keydown');
	// var line = getLine(e.target); // can't -- no node information
	var s = window.rangy.getSelection();
	var line = getLine(s.anchorNode);
	console.log(line + ': [' + s.anchorOffset + ',' + s.focusOffset + '] -> ' + e.which);
	// console.log(e.which + ' we already recorded cursor position, haven\'t we ?');

	// TODO insert using '.innerText += unicode_based_on_e.which'
}

function mouseDownInterceptor(e) {
	// e.preventDefault();
	// console.dir(e);
	// var line = getLine(e.target);
	var s = window.rangy.getSelection();
	var line = getLine(s.anchorNode);
	console.log(line + ':[' + s.anchorOffset + ',' + s.focusOffset + '] ( cursor change -- mouse)');
}